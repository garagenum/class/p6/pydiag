import tkinter as tk
    
def first_screen():
    first_frame.pack()
    tk.Label(first_frame, text="1 screen").pack()

    button_next = tk.Button(first_frame, text='Next', command=second_screen)
    button_next.pack()

def second_screen():

    first_frame.destroy()
    second_frame.pack()
    tk.Label(second_frame, text="2 screen").pack()

window = tk.Tk()
window.title("Salut")
window.geometry('350x200')

first_frame = tk.Frame(window)
second_frame = tk.Frame(window)

first_screen()
window.mainloop()
