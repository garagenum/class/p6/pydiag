# pydiag

A python script for computer diagnosis using tkinter.

## Challenges

- [ ] commenter les fichiers
      On peut s'attaquer directement au commentaire de `screen4.py` ou commencer par un fichier plus simple.
      Il faut trouver le juste milieu entre des commentaires trop longs / trop courts
      Il faut commenter de manière à ce qu'on comprenne le fonctionnement du programme sans connaitre le language.  
- [ ] debuguer screen4.py
      Dans la console, on voit que le print renvoie ̀ type = tuple`.
      Une toute petite correction est nécessaire pour que, comme pour les autres écrans, ce soit bien le type 
      de l'objet tk qui soit affiché.
- [ ] écrire screen6.py
      screen6.py doit contenir les 5 écrans précédents avec, comme pour les autres, un bouton sur l'écran 5
      pour accéder à l'écran 6. Mais les widgets de l'écran 6 doivent être placés avec la méthode `grid()` et pas `pack()`.


## Changelog

## Version v0.1.3
debug

### Version v0.1.1

Update last challenge: use `grid()` instead of `pack()`

### Version v0.1.0

Add files `screen1.py  screen2.py  screen3.py  screen4.py` to show a basic example 
of how to use tkinter to create successive screens.

Each file adds a new screen, showing new python elements.

Each screen is defined in a function ( `def first_screen():`).

The scripts don't uses `Class` object although they should, 
this is an exercice to get some notions of tkinter, functions and object types.
